package ru.tsc.gulin.tm.api.endpoint;

public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {
}
