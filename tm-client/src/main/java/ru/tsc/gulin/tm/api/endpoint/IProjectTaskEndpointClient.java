package ru.tsc.gulin.tm.api.endpoint;

public interface IProjectTaskEndpointClient extends IEndpointClient, IProjectTaskEndpoint {
}
