package ru.tsc.gulin.tm.api.endpoint;

public interface ISystemEndpointClient extends IEndpointClient, ISystemEndpoint {
}
