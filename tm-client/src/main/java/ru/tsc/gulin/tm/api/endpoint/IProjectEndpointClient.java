package ru.tsc.gulin.tm.api.endpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {
}
